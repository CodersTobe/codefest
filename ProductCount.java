
//this was another method we found to take count of objects on a conveyor belt
//here there is a photoelectric sensor on one side of the belt, which emits a thru beam(across the belt) to a receiver on the other end, 
//when an object passes throug the thru-beam, it cuts off the beam, the intensity of light received by the reciever decreases and a signal is generated, due to the signal the 
//count is incremented(initial count =0)

public class productCount {
	
	
	private int count=0;
	

	//an emmitter and receiver are separated.
	//When an object blocks the beam, light emitted from emitter is interrupted and the amount of light intensity received by the receiver becomes less, this is converted into an electrical signal
	//signal takes value 1 and 0.
	
	
	public int countNoOfItems(boolean s) {
		
		while(true) { //while the belt is running.
			if(s==true) {
				return count++; //incrementing count when object passes through the sensor
			}
			else {
				//no action
			}
	}
	
	

	}
}
